#include "catch.hpp"

#include <Utils.hpp>

#include <AnyOf.hpp>
#include <TestClasses/AnyOfTest.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>


TEST_CASE("AnyOf element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_AnyOf.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		AnyOf anyOf = AnyOf(root);
		CHECK(anyOf.getAllOf().size() == 2);
		CHECK(anyOf.getAllOf()[0] != nullptr);
		CHECK(anyOf.getAllOf()[1] != nullptr);

		delete parser;
		delete errHandler;
	}

}

TEST_CASE("AnyOf element evaluates correctly", "[XACML element]") {
	AnyOfTest anyOf = AnyOfTest(3);
	anyOf.evalAllOfReturn = { enumMatch::NOMATCH, enumMatch::INDETERMINATE, enumMatch::MATCH };

	SECTION("If any allOf evaluate to Match then return Match") {
		CHECK(anyOf.evalRequest(Request()) == enumMatch::MATCH);
	}

	SECTION("If none matches and there's at least one indeterminate then return Indeterminate") {
		anyOf.evalAllOfReturn = { enumMatch::NOMATCH, enumMatch::INDETERMINATE, enumMatch::NOMATCH };
		CHECK(anyOf.evalRequest(Request()) == enumMatch::INDETERMINATE);
	}

	SECTION("If all are No match then return No match") {
		anyOf.evalAllOfReturn = { enumMatch::NOMATCH, enumMatch::NOMATCH, enumMatch::NOMATCH };
		CHECK(anyOf.evalRequest(Request()) == enumMatch::NOMATCH);
	}

}