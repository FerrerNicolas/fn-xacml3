#include "catch.hpp"

#include <Utils.hpp>

#include <Attribute.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

TEST_CASE("Attribute element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_Attribute.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		Attribute attr = Attribute(root);
		CHECK(XMLChEqualsString(attr.getAttributeId(), "urn:oasis:names:tc:xacml:1.0:action:action-id"));
		CHECK(XMLChEqualsString(attr.getIncludeInResult(), "false"));
		CHECK(attr.getAttributeValue() != nullptr);

		delete parser;
		delete errHandler;
	}

}