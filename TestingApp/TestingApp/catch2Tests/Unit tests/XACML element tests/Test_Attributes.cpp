#include "catch.hpp"

#include <Utils.hpp>

#include <Attributes.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

TEST_CASE("Attributes element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_Attributes.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		Attributes attr = Attributes(root);
		CHECK(XMLChEqualsString(attr.getCategory(), "urn:oasis:names:tc:xacml:3.0:attribute-category:resource"));
		CHECK(attr.getAttribute() != nullptr);

		delete parser;
		delete errHandler;
	}

}