#include "catch.hpp"

#include <Utils.hpp>

#include <AttributeValue.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

TEST_CASE("AttributeValue element is constructed correctly", "[XACML element]") {

	SECTION("Constructors") {
		AttributeValue attr1 = AttributeValue();
		CHECK(attr1.getDataType() == nullptr);
		CHECK(attr1.getValue() == nullptr);

		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_AttributeValue.xml";
		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		AttributeValue attr2 = AttributeValue(root);
		CHECK(XMLChEqualsString(attr2.getDataType(), "http://www.w3.org/2001/XMLSchema#anyURI"));
		CHECK(XMLChEqualsString(attr2.getValue(), "http://medico.com/record/patient/BartSimpson"));

		delete parser;
		delete errHandler;
	}

}