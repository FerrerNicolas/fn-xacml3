#include "catch.hpp"

#include <Utils.hpp>

#include <Match.hpp>
#include <TestClasses/MatchTest.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>


TEST_CASE("Match element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_Match.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		Match match = Match(root);
		CHECK(XMLChEqualsString(match.getMatchId(), "urn:oasis:names:tc:xacml:1.0:function:anyURI-equal"));
		CHECK(match.getAttributeDesignator() != nullptr);
		CHECK(match.getAttributeValue() != nullptr);
		CHECK(match.getFunction() != nullptr);

		delete parser;
		delete errHandler;
	}

}

TEST_CASE("Match element evaluates correctly", "[XACML element]") {
	MatchTest match = MatchTest();
	match.evalAttributeDesignatorReturn = std::vector<AttributeValue*>(2);
	match.evalFunctionReturn = { enumBool::TRUE, enumBool::TRUE };

	SECTION("If there is an operational error then return Indeterminate") {
		match.throwExceptionFlag = true;
		CHECK(match.evalRequest(Request()) == enumMatch::INDETERMINATE);
	}

	SECTION("If AttributeDesignator evaluates to an empty bag then return false") {
		match.evalAttributeDesignatorReturn = std::vector<AttributeValue*>(0);
		CHECK(match.evalRequest(Request()) == enumMatch::NOMATCH);
	}

	SECTION("If at least one function application returns True then return true") {
		match.evalAttributeDesignatorReturn = std::vector<AttributeValue*>(3);
		match.evalFunctionReturn = { enumBool::INDETERMINATE , enumBool::FALSE , enumBool::TRUE };
		CHECK(match.evalRequest(Request()) == enumMatch::MATCH);
	}

	SECTION("If no function application returns True but at least one returns Indeterminate then return Indeterminate") {
		match.evalAttributeDesignatorReturn = std::vector<AttributeValue*>(3);
		match.evalFunctionReturn = { enumBool::FALSE , enumBool::FALSE , enumBool::INDETERMINATE };
		CHECK(match.evalRequest(Request()) == enumMatch::INDETERMINATE);
	}

	SECTION("If all function applications return False then return false") {
		match.evalAttributeDesignatorReturn = std::vector<AttributeValue*>(3);
		match.evalFunctionReturn = { enumBool::FALSE , enumBool::FALSE , enumBool::FALSE };
		CHECK(match.evalRequest(Request()) == enumMatch::NOMATCH);
	}

}