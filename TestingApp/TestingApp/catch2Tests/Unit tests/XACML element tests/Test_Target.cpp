#include "catch.hpp"

#include <Utils.hpp>

#include <Target.hpp>
#include <TestClasses/TargetTest.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>


TEST_CASE("Target element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_Target.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		Target target = Target(root);
		CHECK(target.getAnyOf().size() == 2);
		CHECK(target.getAnyOf()[0] != nullptr);
		CHECK(target.getAnyOf()[1] != nullptr);

		delete parser;
		delete errHandler;
	}

}

TEST_CASE("Target element evaluates correctly", "[XACML element]") {
	TargetTest target = TargetTest(3);
	target.evalAnyOfReturn = { enumMatch::MATCH, enumMatch::INDETERMINATE, enumMatch::NOMATCH };

	SECTION("An empty taget matches any request") {
		target.setAnyOf(std::vector<AnyOf*>(0));
		CHECK(target.evalRequest(Request()) == enumMatch::MATCH);
	}

	SECTION("If any allOf evaluates to No match then return No match") {
		target.evalAnyOfReturn = { enumMatch::MATCH, enumMatch::INDETERMINATE, enumMatch::NOMATCH };
		CHECK(target.evalRequest(Request()) == enumMatch::NOMATCH);
	}

	SECTION("If there aren't any No match and there's at least one Indeterminate then return Indeterminate") {
		target.evalAnyOfReturn = { enumMatch::MATCH, enumMatch::INDETERMINATE, enumMatch::MATCH };
		CHECK(target.evalRequest(Request()) == enumMatch::INDETERMINATE);
	}

	SECTION("If all are Match then return Match") {
		target.evalAnyOfReturn = { enumMatch::MATCH, enumMatch::MATCH, enumMatch::MATCH };
		CHECK(target.evalRequest(Request()) == enumMatch::MATCH);
	}
	
}