#include "catch.hpp"

#include <Utils.hpp>

#include <Rule.hpp>
#include <TestClasses/RuleTest.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>


TEST_CASE("Rule element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_Rule.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		Rule rule = Rule(root, nullptr);
		CHECK(XMLChEqualsString(rule.getRuleId(), "urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:rule"));
		CHECK(XMLChEqualsString(rule.getEffect(), "Permit"));
		CHECK(rule.getTarget() != nullptr);

		delete parser;
		delete errHandler;
	}

}

TEST_CASE("Rule element evaluates correctly", "[XACML element]") {
	RuleTest rule = RuleTest();
	XMLCh* permitTranscoded = xercesc::XMLString::transcode("Permit");
	XMLCh* denyTranscoded = xercesc::XMLString::transcode("Deny");

	SECTION("Target evaluates to match") {
		rule.evalTargetReturn = enumMatch::MATCH;
		rule.setEffect(permitTranscoded);
		CHECK(rule.evalRequest(Request()) == enumDecision::PERMIT);
		rule.setEffect(denyTranscoded);
		CHECK(rule.evalRequest(Request()) == enumDecision::DENY);
	}

	SECTION("Target evaluates to No match") {
		rule.evalTargetReturn = enumMatch::NOMATCH;
		CHECK(rule.evalRequest(Request()) == enumDecision::NOTAPPLICABLE);
	}
	
	SECTION("Target evaluates to Indeterminate") {
		rule.evalTargetReturn = enumMatch::INDETERMINATE;
		rule.setEffect(permitTranscoded);
		CHECK(rule.evalRequest(Request()) == enumDecision::INDETERMINATE_P);
		rule.setEffect(denyTranscoded);
		CHECK(rule.evalRequest(Request()) == enumDecision::INDETERMINATE_D);
	}
	
}