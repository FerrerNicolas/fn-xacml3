#include "catch.hpp"

#include <Utils.hpp>

#include <Request.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

TEST_CASE("Request element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_Request.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		Request request = Request(root);
		CHECK(XMLChEqualsString(request.getCombinedDecision(), "false"));
		CHECK(XMLChEqualsString(request.getReturnPolicyIdList(), "false"));
		CHECK(request.getAttributes().size() == 2);
		CHECK(request.getAttributes()[0] != nullptr);
		CHECK(request.getAttributes()[1] != nullptr);

		delete parser;
		delete errHandler;
	}

}