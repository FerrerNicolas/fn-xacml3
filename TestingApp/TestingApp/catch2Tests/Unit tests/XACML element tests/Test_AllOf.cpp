#include "catch.hpp"

#include <Utils.hpp>

#include <AllOf.hpp>
#include <TestClasses/AllOfTest.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>


TEST_CASE("AllOf element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_AllOf.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		AllOf allOf = AllOf(root);
		CHECK(allOf.getMatch().size() == 2);
		CHECK(allOf.getMatch()[0] != nullptr);
		CHECK(allOf.getMatch()[1] != nullptr);

		delete parser;
		delete errHandler;
	}

}

TEST_CASE("AllOf element evaluates correctly", "[XACML element]") {
	AllOfTest allOf = AllOfTest(3);
	allOf.evalMatchReturn = { enumMatch::MATCH, enumMatch::INDETERMINATE, enumMatch::NOMATCH};

	SECTION("If any evaluate to false then return No match") {
		CHECK(allOf.evalRequest(Request()) == enumMatch::NOMATCH);
	}

	SECTION("If there aren't any false and there's at least one Indeterminate then return Indeterminate") {
		allOf.evalMatchReturn = { enumMatch::MATCH, enumMatch::INDETERMINATE, enumMatch::MATCH };
		CHECK(allOf.evalRequest(Request()) == enumMatch::INDETERMINATE);
	}

	SECTION("If all evaluate to true then return Match") {
		allOf.evalMatchReturn = { enumMatch::MATCH, enumMatch::MATCH, enumMatch::MATCH };
		CHECK(allOf.evalRequest(Request()) == enumMatch::MATCH);
	}

}