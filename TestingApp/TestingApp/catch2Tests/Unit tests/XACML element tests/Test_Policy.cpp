#include "catch.hpp"

#include <Utils.hpp>

#include <Rule.hpp>
#include <TestClasses/PolicyTest.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>


TEST_CASE("Policy element is constructed correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_Policy.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		Policy policy = Policy(root);
		CHECK(XMLChEqualsString(policy.getPolicyId(), "urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:policy"));
		CHECK(XMLChEqualsString(policy.getRuleCombiningAlgId(), "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-overrides"));
		CHECK(XMLChEqualsString(policy.getVersion(), "1.0"));
		CHECK(policy.getTarget() != nullptr);
		CHECK(policy.getRules().size() == 3);
		CHECK(policy.getRules()[0] != nullptr);
		CHECK(policy.getRules()[1] != nullptr);
		CHECK(policy.getRules()[2] != nullptr);

		delete parser;
		delete errHandler;
	}

}

TEST_CASE("Policy element evaluates correctly", "[XACML element]") {
	PolicyTest policy = PolicyTest();

	SECTION("Target evaluates to No match") {
		policy.evalTargetReturn = enumMatch::NOMATCH;
		CHECK(policy.evalRequest(Request()) == enumDecision::NOTAPPLICABLE);
	}

	SECTION("Target evaluates to Match") {
		policy.evalTargetReturn = enumMatch::MATCH;

		policy.evalCombiningAlgorithmReturn = enumDecision::DENY;
		CHECK(policy.evalRequest(Request()) == enumDecision::DENY);

		policy.evalCombiningAlgorithmReturn = enumDecision::PERMIT;
		CHECK(policy.evalRequest(Request()) == enumDecision::PERMIT);

		policy.evalCombiningAlgorithmReturn = enumDecision::NOTAPPLICABLE;
		CHECK(policy.evalRequest(Request()) == enumDecision::NOTAPPLICABLE);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE_D;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_D);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE_DP;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_DP);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE_P;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_P);
	}

	SECTION("Target evaluates to Indeterminate") {
		policy.evalTargetReturn = enumMatch::INDETERMINATE;

		policy.evalCombiningAlgorithmReturn = enumDecision::DENY;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_D);

		policy.evalCombiningAlgorithmReturn = enumDecision::PERMIT;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_P);

		policy.evalCombiningAlgorithmReturn = enumDecision::NOTAPPLICABLE;
		CHECK(policy.evalRequest(Request()) == enumDecision::NOTAPPLICABLE);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_DP);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE_D;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_D);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE_DP;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_DP);

		policy.evalCombiningAlgorithmReturn = enumDecision::INDETERMINATE_P;
		CHECK(policy.evalRequest(Request()) == enumDecision::INDETERMINATE_P);

	}

}

