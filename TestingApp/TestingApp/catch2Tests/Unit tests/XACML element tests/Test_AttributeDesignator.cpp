#include "catch.hpp"

#include <Utils.hpp>

#include <AttributeDesignator.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>

TEST_CASE("AttributeDesignator element is constructed and evaluated correctly", "[XACML element]") {

	SECTION("Constructor") {
		const char* xmlFile = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_AttributeDesignator.xml";

		std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parse = simpleXMLFileParse(xmlFile);
		xercesc::XercesDOMParser* parser = parse.first;
		xercesc::ErrorHandler* errHandler = parse.second;
		xercesc::DOMDocument* doc = parser->getDocument();
		xercesc::DOMElement* root = doc->getDocumentElement();

		AttributeDesignator attributeDesignator = AttributeDesignator(root);
		CHECK(XMLChEqualsString(attributeDesignator.getAttributeId(), "urn:oasis:names:tc:xacml:1.0:resource:resource-id"));
		CHECK(XMLChEqualsString(attributeDesignator.getDataType(), "http://www.w3.org/2001/XMLSchema#anyURI"));
		CHECK(XMLChEqualsString(attributeDesignator.getMustBePresent(), "false"));
		CHECK(XMLChEqualsString(attributeDesignator.getCategory(), "urn:oasis:names:tc:xacml:3.0:attribute-category:resource"));

		SECTION("Evaluation") {
			// Loading the request
			const char* xmlFileRequest = ".\\catch2Tests\\Unit tests\\XACML element tests\\Test_AttributeDesignator_Request.xml";

			std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > parseRequest = simpleXMLFileParse(xmlFileRequest);
			xercesc::XercesDOMParser* parserRequest = parseRequest.first;
			xercesc::ErrorHandler* errHandlerRequest = parseRequest.second;
			xercesc::DOMDocument* docRequest = parserRequest->getDocument();
			xercesc::DOMElement* rootRequest = docRequest->getDocumentElement();
			Request request = Request(rootRequest);

			// Evaluating
			std::vector<AttributeValue*> attributeValues = attributeDesignator.getAttributeValues(request);

			REQUIRE(attributeValues.size() == 1);
			REQUIRE(attributeValues[0] != nullptr);
			CHECK(XMLChEqualsString(attributeValues[0]->getDataType(), "http://www.w3.org/2001/XMLSchema#anyURI"));
			CHECK(XMLChEqualsString(attributeValues[0]->getValue(), "http://medico.com/record/patient/BartSimpson"));

			delete parserRequest;
			delete errHandlerRequest;
		}

		delete parser;
		delete errHandler;
	}

}