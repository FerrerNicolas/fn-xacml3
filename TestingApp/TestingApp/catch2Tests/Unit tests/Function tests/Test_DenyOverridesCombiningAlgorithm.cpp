#include "catch.hpp"

#include <xercesc/util/XMLString.hpp>

#include <DenyOverridesCombiningAlgorithm.hpp>
#include <TestClasses/Function Test Classes/DenyOverridesCombiningAlgorithmTest.hpp>

TEST_CASE("Function DenyOverridesCombiningAlgorithm evaluates correctly", "[Combining Algorithm]") {

	DenyOverridesCombiningAlgorithmTest func;
	enumDecision result;
	

	SECTION("If any decision is \"Deny\", the result is \"Deny\"") {
		func.evalRuleReturn = { enumDecision::NOTAPPLICABLE, enumDecision::INDETERMINATE_P, enumDecision::PERMIT, enumDecision::INDETERMINATE_D, enumDecision::INDETERMINATE_DP, enumDecision::DENY };

		// The contents of the Rules and the Request don't matter since they aren't used
		enumDecision result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::DENY);

	}

	SECTION("Otherwise, if any decision is \"Indeterminate{ DP }\", the result is \"Indeterminate{ DP }\"") {
		func.evalRuleReturn = { enumDecision::NOTAPPLICABLE, enumDecision::INDETERMINATE_P, enumDecision::PERMIT, enumDecision::PERMIT, enumDecision::INDETERMINATE_DP, enumDecision::PERMIT };

		// The contents of the Rules and the Request don't matter since they aren't used
		enumDecision result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::INDETERMINATE_DP);

	}

	SECTION("Otherwise, if any decision is \"Indeterminate{ D }\" and another decision is \"Indeterminate{ P } or Permit, the result is \"Indeterminate{ DP }\"") {
		func.evalRuleReturn = { enumDecision::NOTAPPLICABLE, enumDecision::PERMIT, enumDecision::PERMIT, enumDecision::PERMIT, enumDecision::INDETERMINATE_D, enumDecision::PERMIT };

		// The contents of the Rules and the Request don't matter since they aren't used
		enumDecision result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::INDETERMINATE_DP);


		func.evalRuleReturn = { enumDecision::NOTAPPLICABLE, enumDecision::INDETERMINATE_P, enumDecision::INDETERMINATE_P, enumDecision::INDETERMINATE_P, enumDecision::INDETERMINATE_D, enumDecision::NOTAPPLICABLE };

		// The contents of the Rules and the Request don't matter since they aren't used
		result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::INDETERMINATE_DP);

	}

	SECTION("Otherwise, if any decision is \"Indeterminate{ D }\", the result is \"Indeterminate{ D }\"") {
		func.evalRuleReturn = { enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::INDETERMINATE_D, enumDecision::NOTAPPLICABLE };

		// The contents of the Rules and the Request don't matter since they aren't used
		enumDecision result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::INDETERMINATE_D);

	}

	SECTION("Otherwise, if any decision is \"Permit\", the result is \"Permit\"") {
		func.evalRuleReturn = { enumDecision::INDETERMINATE_P, enumDecision::NOTAPPLICABLE, enumDecision::PERMIT, enumDecision::INDETERMINATE_P, enumDecision::INDETERMINATE_P, enumDecision::NOTAPPLICABLE };

		// The contents of the Rules and the Request don't matter since they aren't used
		enumDecision result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::PERMIT);

	}

	SECTION("Otherwise, if any decision is \"Indeterminate{ P }\", the result is \"Indeterminate{ P }\"") {
		func.evalRuleReturn = { enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::INDETERMINATE_P, enumDecision::INDETERMINATE_P, enumDecision::NOTAPPLICABLE };

		// The contents of the Rules and the Request don't matter since they aren't used
		enumDecision result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::INDETERMINATE_P);

	}

	SECTION("Otherwise, the result is \"NotApplicable\"") {
		func.evalRuleReturn = { enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE, enumDecision::NOTAPPLICABLE };

		// The contents of the Rules and the Request don't matter since they aren't used
		enumDecision result = func.evalRequest(std::vector<Rule*>(func.evalRuleReturn.size()), Request());

		CHECK(result == enumDecision::NOTAPPLICABLE);

	}


}