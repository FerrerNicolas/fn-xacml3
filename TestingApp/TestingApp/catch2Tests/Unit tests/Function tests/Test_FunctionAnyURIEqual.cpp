#include "catch.hpp"

#include <xercesc/util/XMLString.hpp>

#include <FunctionAnyURIEqual.hpp>
#include <AttributeValue.hpp>

TEST_CASE("Function AnyURIEqual evaluates correctly", "[Equality predicates]") {

	FunctionAnyURIEqual func;

	SECTION("Comparing equal URIs") {
		AttributeValue attr1;
		attr1.setDataType(xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI"));
		attr1.setValue(xercesc::XMLString::transcode("VALUE 1"));
		AttributeValue attr2;
		attr2.setDataType(xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI"));
		attr2.setValue(xercesc::XMLString::transcode("VALUE 1"));
		CHECK(func.eval(&attr1, &attr2) == enumBool::TRUE);
		CHECK(func.eval(&attr2, &attr1) == enumBool::TRUE);
		CHECK(func.eval(&attr1, &attr1) == enumBool::TRUE);

	}

	SECTION("Comparing different value URIs") {
		AttributeValue attr1;
		attr1.setDataType(xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI"));
		attr1.setValue(xercesc::XMLString::transcode("VALUE 1"));
		AttributeValue attr2;
		attr2.setDataType(xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI"));
		attr2.setValue(xercesc::XMLString::transcode("VALUE 2"));
		CHECK(func.eval(&attr1, &attr2) == enumBool::FALSE);
		CHECK(func.eval(&attr2, &attr1) == enumBool::FALSE);

	}
	
	SECTION("Comparing different length URIs") {
		AttributeValue attr1;
		attr1.setDataType(xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI"));
		attr1.setValue(xercesc::XMLString::transcode("VALUE 1"));
		AttributeValue attr2;
		attr2.setDataType(xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI"));
		attr2.setValue(xercesc::XMLString::transcode("VALUE"));
		CHECK(func.eval(&attr1, &attr2) == enumBool::FALSE);
		CHECK(func.eval(&attr2, &attr1) == enumBool::FALSE);

	}

}