#include "catch.hpp"

#include <RuleCombiningAlgorithmSelector.hpp>
#include <xercesc/util/XMLString.hpp>

TEST_CASE("RuleCombiningAlgorithmSelector returns instance of the correct function", "[Auxiliary element]") {

	RuleCombiningAlgorithm* func;
	
	XMLCh* funcURI = xercesc::XMLString::transcode("urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-overrides");
	func = RuleCombiningAlgorithmSelector::getRuleCombiningAlgorithm(funcURI);
	// Dynamic_cast will return a nullptr if the conversion fails; and a pointer of the specified type otherwise
	CHECK(dynamic_cast<DenyOverridesCombiningAlgorithm*>(func));
	xercesc::XMLString::release(&funcURI);

}