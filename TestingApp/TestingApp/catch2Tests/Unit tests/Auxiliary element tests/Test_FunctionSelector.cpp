#include "catch.hpp"

#include <FunctionSelector.hpp>
#include <xercesc/util/XMLString.hpp>

TEST_CASE("FunctionSelector returns instance of the correct function", "[Auxiliary element]") {

	TwoArgBoolFunction* func;
	
	XMLCh* funcURI = xercesc::XMLString::transcode("urn:oasis:names:tc:xacml:1.0:function:anyURI-equal");
	func = FunctionSelector::getTwoArgBoolFunction(funcURI);
	// Dynamic_cast will return a nullptr if the conversion fails; and a pointer of the specified type otherwise
	CHECK(dynamic_cast<FunctionAnyURIEqual*>(func));
	xercesc::XMLString::release(&funcURI);

}