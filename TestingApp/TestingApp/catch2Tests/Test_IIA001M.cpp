#include "catch.hpp"

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>

#include <iostream>

#include <Utils.hpp>

#include <Policy.hpp>





TEST_CASE("Reading Policy IIA001PolicyM", "[XACML Reading]") {

	const char* xmlFile = "..\\tests\\XACML3tests\\IIA\\IIA001PolicyM.xacml3.xml";


	xercesc::XercesDOMParser* parser = new xercesc::XercesDOMParser();
	parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);

	xercesc::ErrorHandler* errHandler = (xercesc::ErrorHandler*) new xercesc::HandlerBase();
	parser->setErrorHandler(errHandler);

	try {
		parser->parse(xmlFile);
	}
	catch (const xercesc::XMLException & toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.getMessage());
		std::cout << "Exception message is: \n"
			<< message << "\n";
		xercesc::XMLString::release(&message);
		return;
	}
	catch (const xercesc::DOMException & toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.msg);
		std::cout << "Exception message is: \n"
			<< message << "\n";
		xercesc::XMLString::release(&message);
		return;
	}
	catch (...) {
		std::cout << "Unexpected Exception \n";
		return;
	}

	xercesc::DOMDocument* doc = parser->getDocument();
	xercesc::DOMElement* root = doc->getDocumentElement();

	SECTION("Constructing the Policy object") {
		Policy policy = Policy(root);

		// Require that the Policy attributes match the document
		REQUIRE(XMLChEqualsString(policy.getPolicyId(), "urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:policy"));
		REQUIRE(XMLChEqualsString(policy.getRuleCombiningAlgId(), "urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-overrides"));
		REQUIRE(XMLChEqualsString(policy.getVersion(), "1.0"));

		// Require that the various child elements have been constructed and that it was done so correctly
		// Target
		REQUIRE(policy.getTarget() != nullptr);

		// Rules (In this policy just 1)
		std::vector<Rule*> rules = policy.getRules();
		REQUIRE(rules.size() == 1);
		Rule* rule = rules[0];
		REQUIRE(rule != nullptr);
		REQUIRE(XMLChEqualsString(rule->getEffect(), "Permit"));
		REQUIRE(XMLChEqualsString(rule->getRuleId(), "urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:rule"));
		Target* ruleTarget = rule->getTarget();
		REQUIRE(ruleTarget != nullptr);

			// Require proper construction of the Target of the rule
			std::vector<AnyOf*> anyOf = ruleTarget->getAnyOf();
			REQUIRE(anyOf.size() == 1);
			AnyOf* anyOfelem = anyOf[0];
			REQUIRE(anyOfelem != nullptr);
				
				// Require proper construction of the AnyOf of the target
				std::vector<AllOf*> allOf = anyOfelem->getAllOf();
				REQUIRE(allOf.size() == 1);
				AllOf* allOfelem = allOf[0];
				REQUIRE(allOfelem != nullptr);

					// Require proper construction of the AllOf of the anyOf
					std::vector<Match*> match = allOfelem->getMatch();
					REQUIRE(match.size() == 1);
					Match* matchelem = match[0];
					REQUIRE(matchelem != nullptr);
					REQUIRE(XMLChEqualsString(matchelem->getMatchId(), "urn:oasis:names:tc:xacml:1.0:function:anyURI-equal"));
					AttributeValue* attributeValue = matchelem->getAttributeValue();
					REQUIRE(attributeValue != nullptr);

						// Require proper construction of the AttributeValue of the match
						REQUIRE(XMLChEqualsString(attributeValue->getDataType(), "http://www.w3.org/2001/XMLSchema#anyURI"));
						REQUIRE(XMLChEqualsString(attributeValue->getValue(), "http://medico.com/record/patient/BartSimpson"));

					AttributeDesignator* attributeDesignator = matchelem->getAttributeDesignator();
					REQUIRE(attributeDesignator != nullptr);
						
						// Require proper construction of the AttributeDesignator of the match
						REQUIRE(XMLChEqualsString(attributeDesignator->getAttributeId(), "urn:oasis:names:tc:xacml:1.0:resource:resource-id"));
						REQUIRE(XMLChEqualsString(attributeDesignator->getDataType(), "http://www.w3.org/2001/XMLSchema#anyURI"));
						REQUIRE(XMLChEqualsString(attributeDesignator->getMustBePresent(), "false"));
						REQUIRE(XMLChEqualsString(attributeDesignator->getCategory(), "urn:oasis:names:tc:xacml:3.0:attribute-category:resource"));


	}
	
	// REQUIRE(XMLChEqualsString(xmlch, str));


	delete parser;
	delete errHandler;

}

#include <Request.hpp>

TEST_CASE("Reading Request IIA001RequestM", "[XACML Reading]") {

	const char* xmlFile = "..\\tests\\XACML3tests\\IIA\\IIA001RequestM.xacml3.xml";


	xercesc::XercesDOMParser* parser = new xercesc::XercesDOMParser();
	parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);

	xercesc::ErrorHandler* errHandler = (xercesc::ErrorHandler*) new xercesc::HandlerBase();
	parser->setErrorHandler(errHandler);

	try {
		parser->parse(xmlFile);
	}
	catch (const xercesc::XMLException & toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.getMessage());
		std::cout << "Exception message is: \n"
			<< message << "\n";
		xercesc::XMLString::release(&message);
		return;
	}
	catch (const xercesc::DOMException & toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.msg);
		std::cout << "Exception message is: \n"
			<< message << "\n";
		xercesc::XMLString::release(&message);
		return;
	}
	catch (...) {
		std::cout << "Unexpected Exception \n";
		return;
	}

	xercesc::DOMDocument* doc = parser->getDocument();
	xercesc::DOMElement* root = doc->getDocumentElement();

	SECTION("Constructing the Request object") {
		Request request = Request(root);

		// Require that the Request attributes match the document
		REQUIRE(XMLChEqualsString(request.getReturnPolicyIdList(), "false"));
		REQUIRE(XMLChEqualsString(request.getCombinedDecision(), "false"));

		// Require that the various child elements have been constructed and that it was done so correctly
		// Attributes
		std::vector<Attributes*> attributes = request.getAttributes();
		REQUIRE(attributes.size() == 4);

		// Checks for the first 3 as they have the same structure
		std::string categories[3] = {
			"urn:oasis:names:tc:xacml:1.0:subject-category:access-subject",
			"urn:oasis:names:tc:xacml:3.0:attribute-category:resource",
			"urn:oasis:names:tc:xacml:3.0:attribute-category:action"
		};
		std::string includeInResults[3] = {
			"false",
			"false",
			"false",
		};
		std::string attributeIds[3] = {
			"urn:oasis:names:tc:xacml:1.0:subject:subject-id",
			"urn:oasis:names:tc:xacml:1.0:resource:resource-id",
			"urn:oasis:names:tc:xacml:1.0:action:action-id",
		};
		std::string dataTypes[3] = {
			"http://www.w3.org/2001/XMLSchema#string",
			"http://www.w3.org/2001/XMLSchema#anyURI",
			"http://www.w3.org/2001/XMLSchema#string",
		};
		std::string textValues[3] = {
			"Julius Hibbert",
			"http://medico.com/record/patient/BartSimpson",
			"read",
		};

		for (int i = 0; i < 3; i++) {
			REQUIRE(attributes[i] != nullptr);
			REQUIRE(XMLChEqualsString(attributes[i]->getCategory(), categories[i]));
			Attribute* attribute = attributes[i]->getAttribute();
			REQUIRE(attribute != nullptr);
			REQUIRE(XMLChEqualsString(attribute->getIncludeInResult(), includeInResults[i]));
			REQUIRE(XMLChEqualsString(attribute->getAttributeId(), attributeIds[i]));
			AttributeValue* attributeValue = attribute->getAttributeValue();
			REQUIRE(attributeValue != nullptr);
			REQUIRE(XMLChEqualsString(attributeValue->getDataType(), dataTypes[i]));
			REQUIRE(XMLChEqualsString(attributeValue->getValue(), textValues[i]));
		}


		// Attributes number 4
		REQUIRE(attributes[3] != nullptr);
		REQUIRE(XMLChEqualsString(attributes[3]->getCategory(), "urn:oasis:names:tc:xacml:3.0:attribute-category:environment"));


	}

	// REQUIRE(XMLChEqualsString(xmlch, str));


	delete parser;
	delete errHandler;

}


TEST_CASE("Evaluating Request IIA001RequestM", "[XACML Reading]") {

	// Reading the XML files

	const char* xmlFilePolicy = "..\\tests\\XACML3tests\\IIA\\IIA001PolicyM.xacml3.xml";
	const char* xmlFileRequest = "..\\tests\\XACML3tests\\IIA\\IIA001RequestM.xacml3.xml";

	std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > policyParse = simpleXMLFileParse(xmlFilePolicy);
	xercesc::XercesDOMParser* parserPolicy = policyParse.first;
	xercesc::ErrorHandler* errHandlerPolicy = policyParse.second;
	xercesc::DOMDocument* docPolicy = parserPolicy->getDocument();
	xercesc::DOMElement* rootPolicy = docPolicy->getDocumentElement();

	std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > requestParse = simpleXMLFileParse(xmlFileRequest);
	xercesc::XercesDOMParser* parserRequest = requestParse.first;
	xercesc::ErrorHandler* errHandlerRequest = requestParse.second;
	xercesc::DOMDocument* docRequest = parserRequest->getDocument();
	xercesc::DOMElement* rootRequest = docRequest->getDocumentElement();

	// Constructing the Policy 
	Policy policy = Policy(rootPolicy);

	// Constructing Request
	Request request = Request(rootRequest);

	// Require that the evaluation equals "Permit"
	enumDecision result = policy.evalRequest(request);
	REQUIRE(result == enumDecision::PERMIT);

	delete parserPolicy;
	delete errHandlerPolicy;
	delete parserRequest;
	delete errHandlerRequest;

}