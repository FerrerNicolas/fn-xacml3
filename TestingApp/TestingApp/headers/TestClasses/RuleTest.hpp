#pragma once

#ifndef RULETEST_H
#define RULETEST_H

#include <Rule.hpp>

// Class used for faking certain characteristics of the base class for testing purposes
class RuleTest : public Rule {

protected:
	// Overriding the functions that evaluates its components so as to fake the results for testing
	enumMatch evalTarget(Request request) override;

public:
	// Use the parent's constructor as we don't need to modify any attributes for testing
	using Rule::Rule;
	enumMatch evalTargetReturn;
};

#endif