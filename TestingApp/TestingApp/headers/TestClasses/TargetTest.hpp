#pragma once

#ifndef TARGETTEST_H
#define TARGETTEST_H

#include <Target.hpp>

// Class used for faking certain characteristics of the base class for testing purposes
class TargetTest : public Target {

protected:
	// Overriding the function that evaluates its components so as to fake the results for testing
	enumMatch evalAnyOf(int index, Request request) override;

public:
	// Constructor calls the parent's constructor then fakes the vector size
	TargetTest(xercesc::DOMNode* targetNode);
	TargetTest(int vectorSize);
	std::vector<enumMatch> evalAnyOfReturn;
};

#endif