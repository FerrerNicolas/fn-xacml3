#pragma once

#ifndef POLICYTEST_H
#define POLICYTEST_H

#include <Policy.hpp>

// Class used for faking certain characteristics of the base class for testing purposes
class PolicyTest : public Policy {

protected:
	// Overriding the functions that evaluates its components so as to fake the results for testing
	enumMatch evalTarget(Request request) override;
	enumDecision evalCombiningAlgorithm(Request request) override;

public:
	// Use the parent's constructor as we don't need to modify any attributes for testing
	using Policy::Policy;
	enumMatch evalTargetReturn;
	enumDecision evalCombiningAlgorithmReturn;
};

#endif