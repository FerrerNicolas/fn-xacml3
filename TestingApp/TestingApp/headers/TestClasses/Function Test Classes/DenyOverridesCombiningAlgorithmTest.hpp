#pragma once

#ifndef DENYOVERRIDESCOMBININGALGORITHMTEST_H
#define DENYOVERRIDESCOMBININGALGORITHMTEST_H

#include <DenyOverridesCombiningAlgorithm.hpp>

// Class used for faking certain characteristics of the base class for testing purposes
class DenyOverridesCombiningAlgorithmTest : public DenyOverridesCombiningAlgorithm {

protected:
	// Overriding the function that evaluates its components so as to fake the results for testing
	enumDecision evalRule(std::vector<Rule*> ruleVector, Request request, int index) const override;

public:
	std::vector<enumDecision> evalRuleReturn;
};

#endif