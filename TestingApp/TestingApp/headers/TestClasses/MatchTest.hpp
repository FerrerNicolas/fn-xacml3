#pragma once

#ifndef MATCHTEST_H
#define MATCHTEST_H

#include <Match.hpp>

// Class used for faking certain characteristics of the base class for testing purposes
class MatchTest : public Match {

protected:
	// Overriding the functions that evaluates its components so as to fake the results for testing
	std::vector<AttributeValue*> evalAttributeDesignator(Request request) override;
	enumBool evalFunction(std::vector<AttributeValue*> bag, int index) override;

public:
	// Use the parent's constructor as we don't need to modify any attributes for testing
	using Match::Match;
	std::vector<AttributeValue*> evalAttributeDesignatorReturn;
	std::vector<enumBool> evalFunctionReturn;
	bool throwExceptionFlag = false;
};

#endif