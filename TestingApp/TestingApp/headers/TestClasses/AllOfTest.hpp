#pragma once

#ifndef ALLOFTEST_H
#define ALLOFTEST_H

#include <AllOf.hpp>

// Class used for faking certain characteristics of the base class for testing purposes
class AllOfTest : public AllOf {

protected:
	// Overriding the functions that evaluates its components so as to fake the results for testing
	enumMatch evalMatch(Request request, int index) override;

public:
	// Constructor calls the parent's constructor then fakes the vector size
	AllOfTest(int vectorSize);
	AllOfTest(xercesc::DOMNode* allOfNode);
	std::vector<enumMatch> evalMatchReturn;
};

#endif