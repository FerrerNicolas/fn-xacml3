#pragma once

#ifndef ANYOFTEST_H
#define ANYOFTEST_H

#include <AnyOf.hpp>

// Class used for faking certain characteristics of the base class for testing purposes
class AnyOfTest : public AnyOf {

protected:
	// Overriding the functions that evaluates its components so as to fake the results for testing
	enumMatch evalAllOf(Request request, int index) const override;

public:
	// Constructor calls the parent's constructor then fakes the vector size
	AnyOfTest(int vectorSize);
	AnyOfTest(xercesc::DOMNode* allOfNode);
	std::vector<enumMatch> evalAllOfReturn;
};

#endif