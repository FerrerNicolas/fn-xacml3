
//#define MANUAL_TESTING

#ifndef MANUAL_TESTING

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>

int main(int argc, char* argv[]) {
	// global setup...

	// Initializing xercesc XMLPlatformUtils
	try {
		xercesc::XMLPlatformUtils::Initialize();
	}
	catch (const xercesc::XMLException & toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.getMessage());
		std::cout << "Error during XMLPlatformUtils initialization! :\n" 
			<< message << "\n";
		xercesc::XMLString::release(&message);
		return 1;
	}

	int result = Catch::Session().run(argc, argv);

	// global clean-up...

	return result;
}



#endif

#ifdef MANUAL_TESTING

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include <iostream>

#include <xercesc/framework/StdOutFormatTarget.hpp>

//#include <FunctionAnyURIEqual.hpp>
//#include <FunctionSelector.hpp>
#include <ElementAttributes.hpp>
#include <Policy.hpp>
#include <Request.hpp>


using namespace std;
using namespace xercesc;

int main()
{

	//const char* xmlFile = "C:\\Users\\guill\\Documents\\GitHub\\fn-xacml3\\TestingApp\\tests\\XACML3tests\\IIA\\IIA001Policy.xacml3.xml";

	const char* xmlFile = "C:\\Users\\guill\\Documents\\GitHub\\fn-xacml3\\TestingApp\\tests\\XACML3tests\\IIA\\IIA001PolicyM.xacml3.xml";
	const char* xmlFile2 = "C:\\Users\\guill\\Documents\\GitHub\\fn-xacml3\\TestingApp\\tests\\XACML3tests\\IIA\\IIA001RequestM.xacml3.xml";

	try {
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException & toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Error during initialization! :\n"
			<< message << "\n";
		XMLString::release(&message);
		return 1;
	}


	XercesDOMParser* parser = new XercesDOMParser();
	parser->setValidationScheme(XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);    // optional

	// Otherwise Xerces by default considers white spaces (tabs, end lines and spaces) as textNodes
	// parser->setIncludeIgnorableWhitespace(false);

	ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
	parser->setErrorHandler(errHandler);

	
	
	try {
		parser->parse(xmlFile);
	}
	catch (const XMLException & toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (const DOMException & toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		cout << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (...) {
		cout << "Unexpected Exception \n";
		return -1;
	}
	

	//XMLCh tempStr[3] = { chLatin_L, chLatin_S, chNull };
	//DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(tempStr);
	//DOMLSSerializer* theSerializer = ((DOMImplementationLS*)impl)->createLSSerializer();
	//DOMLSOutput* theOutputDesc = ((DOMImplementationLS*)impl)->createLSOutput();


	//XMLFormatTarget* myFormTarget;
	//myFormTarget = new StdOutFormatTarget();
	//theOutputDesc->setByteStream(myFormTarget);

	// get the DOM representation
	DOMDocument* doc = parser->getDocument();

	DOMElement* root = doc->getDocumentElement();

	/*
	DOMNode* child1 = root->getFirstChild();
	//DOMNode* child2 = child1->getFirstChild();
	
	const XMLCh* name = child1->getNodeName();
	const XMLCh* val = child1->getNodeValue();

	XMLCh* a = theSerializer->writeToString(child1);
	
	//cout << child1->getNodeType() << endl;

	char* test = XMLString::transcode(name);

	cout << test << endl;
	cout << *name << endl;
	cout << *val << endl;

	int size = root->getChildElementCount();
	cout << "size: " << size << endl;
	DOMNodeList* children = root->getChildNodes();
	cout << "length: " << children->getLength() << endl;
	for (int i = 0; i < children->getLength(); i++) {
		DOMNode* currChild = children->item(i);
		XMLCh* out = theSerializer->writeToString(currChild);
		char* out2 = XMLString::transcode(out);
		cout << "Child " << i << endl;
		cout << out2 << endl;
	}
	
	DOMNode* attrs = children->item(1);
	DOMNamedNodeMap* attrs_attributes = attrs->getAttributes();
	for (int i = 0; i < attrs_attributes->getLength(); i++) {
		DOMNode* currAttribute = attrs_attributes->item(i);
		const XMLCh* currAttributeName = currAttribute->getNodeName();
		const XMLCh* currAttributeValue = currAttribute->getNodeValue();
		char* currAttributeNameTranscoded = XMLString::transcode(currAttributeName);
		char* currAttributeValueTranscoded = XMLString::transcode(currAttributeValue);
		cout << "Attribute " << i << endl;
		cout << "	Name: "<< currAttributeNameTranscoded << endl;
		cout << "	Value: " << currAttributeValueTranscoded << endl;
		cout << endl;
	}

	children = attrs->getChildNodes();
	cout << "length: " << children->getLength() << endl;
	for (int i = 0; i < children->getLength(); i++) {
		DOMNode* currChild = children->item(i);
		XMLCh* out = theSerializer->writeToString(currChild);
		char* out2 = XMLString::transcode(out);
		cout << "Child " << i << endl;
		cout << out2 << endl;
	}

	DOMNode* attr = children->item(1);
	DOMNamedNodeMap* attr_attributes = attr->getAttributes();
	for (int i = 0; i < attr_attributes->getLength(); i++) {
		DOMNode* currAttribute = attr_attributes->item(i);
		const XMLCh* currAttributeName = currAttribute->getNodeName();
		const XMLCh* currAttributeValue = currAttribute->getNodeValue();
		char* currAttributeNameTranscoded = XMLString::transcode(currAttributeName);
		char* currAttributeValueTranscoded = XMLString::transcode(currAttributeValue);
		cout << "Attribute " << i << endl;
		cout << "	Name: " << currAttributeNameTranscoded << endl;
		cout << "	Value: " << currAttributeValueTranscoded << endl;
		cout << endl;
	}

	string test1 = "test";
	string test2 = "test";
	string test3 = "test ";
	string test4 = "tset";
	
	XMLCh* test1x = XMLString::transcode(test1.c_str());
	XMLCh* test2x = XMLString::transcode(test2.c_str());
	XMLCh* test3x = XMLString::transcode(test3.c_str());
	XMLCh* test4x = XMLString::transcode(test4.c_str());

	cout << "Direct comparison:" << endl;
	cout << (test1x == test1x) << (test1x == test2x) << (test1x == test3x) << (test1x == test4x) << endl;

	cout << "Function comparison:" << endl;
	cout << XMLString::equals(test1x, test1x) << XMLString::equals(test1x, test2x) << XMLString::equals(test1x, test3x) << XMLString::equals(test1x, test4x) << endl;

	cout << endl;
	ElementAttributes elementAttributes;
	elementAttributes.set(test1x, test4x);
	string test5 = "A very very long string ~€¬¬€#@";
	XMLCh* test5x = XMLString::transcode(test5.c_str());
	elementAttributes.set(test4x, test5x);

	const XMLCh* res1 = elementAttributes.getValue(test2x);
	const XMLCh* res2 = elementAttributes.getValue(test4x);

	cout << XMLString::equals(res1, test4x) << ", " << XMLString::equals(res2, test5x) << endl;
	*/

	Policy policy = Policy(root);

	//theSerializer->write(child1, theOutputDesc);

	//theOutputDesc->release();
	//theSerializer->release();


	XercesDOMParser* parser2 = new XercesDOMParser();
	parser2->setValidationScheme(XercesDOMParser::Val_Always);
	parser2->setDoNamespaces(true);    // optional

	ErrorHandler* errHandler2 = (ErrorHandler*) new HandlerBase();
	parser2->setErrorHandler(errHandler2);

	try {
		parser2->parse(xmlFile2);
	}
	catch (const XMLException & toCatch) {
		char* message = XMLString::transcode(toCatch.getMessage());
		cout << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (const DOMException & toCatch) {
		char* message = XMLString::transcode(toCatch.msg);
		cout << "Exception message is: \n"
			<< message << "\n";
		XMLString::release(&message);
		return -1;
	}
	catch (...) {
		cout << "Unexpected Exception \n";
		return -1;
	}

	// get the DOM representation
	DOMDocument* doc2 = parser2->getDocument();
	DOMElement* root2 = doc2->getDocumentElement();

	Request request(root2);

	bool decision = policy.eval(root2);

	if (decision)
		std::cout << "Permit" << endl;
	else
		std::cout << "Deny" << endl;

	delete parser;
	delete errHandler;
	delete parser2;
	delete errHandler2;
	return 0;

}

#endif