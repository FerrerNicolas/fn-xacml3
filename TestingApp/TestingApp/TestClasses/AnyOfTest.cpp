#include "..\headers\TestClasses\AnyOfTest.hpp"

enumMatch AnyOfTest::evalAllOf(Request request, int index) const
{
	return evalAllOfReturn[index];
}

AnyOfTest::AnyOfTest(int vectorSize) : AnyOf()
{
	allOf.resize(vectorSize);
	evalAllOfReturn = std::vector<enumMatch>(vectorSize);
}

AnyOfTest::AnyOfTest(xercesc::DOMNode* allOfNode) : AnyOf(allOfNode)
{
}
