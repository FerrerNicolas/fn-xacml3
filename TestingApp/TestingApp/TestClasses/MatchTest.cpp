#include "..\headers\TestClasses\MatchTest.hpp"

std::vector<AttributeValue*> MatchTest::evalAttributeDesignator(Request request)
{
	if (throwExceptionFlag)
		throw std::exception();
	return evalAttributeDesignatorReturn;
}

enumBool MatchTest::evalFunction(std::vector<AttributeValue*> bag, int index)
{
	return evalFunctionReturn[index];
}
