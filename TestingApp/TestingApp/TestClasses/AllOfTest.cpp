#include "..\headers\TestClasses\AllOfTest.hpp"

enumMatch AllOfTest::evalMatch(Request request, int index)
{
	return evalMatchReturn[index];
}

AllOfTest::AllOfTest(int vectorSize) : AllOf()
{
	this->match.resize(vectorSize);
	evalMatchReturn = std::vector<enumMatch>(vectorSize);
}

AllOfTest::AllOfTest(xercesc::DOMNode* allOfNode) : AllOf(allOfNode)
{
}
