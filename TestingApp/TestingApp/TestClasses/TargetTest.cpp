#include "..\headers\TestClasses\TargetTest.hpp"

enumMatch TargetTest::evalAnyOf(int index, Request request)
{
	return evalAnyOfReturn[index];
}

TargetTest::TargetTest(xercesc::DOMNode* targetNode) : Target(targetNode) 
{
}

TargetTest::TargetTest(int vectorSize)
{
	this->anyOf.resize(vectorSize);
	evalAnyOfReturn = std::vector<enumMatch>(vectorSize);
}
