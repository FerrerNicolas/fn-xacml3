#include "Match.hpp"
#include <Utils.hpp>
#include <FunctionSelector.hpp>
#include <vector>

const XMLCh* Match::matchIdKey;

void Match::initKeys()
{
	if (matchIdKey == nullptr) {
		matchIdKey = xercesc::XMLString::transcode("MatchId");
	}
}

std::vector<AttributeValue*> Match::evalAttributeDesignator(Request request)
{
	return attributeDesignator->getAttributeValues(request);
}

enumBool Match::evalFunction(std::vector<AttributeValue*> bag, int index)
{
	return function->eval(attributeValue, bag[index]);
}

Match::Match(xercesc::DOMNode* matchNode){
	initKeys();
	elementAttributes = ElementAttributes(matchNode);
	attributeValue = new AttributeValue(getChildWithName(matchNode, "AttributeValue"));
	attributeDesignator = new AttributeDesignator(getChildWithName(matchNode, "AttributeDesignator"));
	function = FunctionSelector::getTwoArgBoolFunction(elementAttributes.getValue(matchIdKey));
}

Match::Match()
{
	initKeys();
}

// Section 7.6 Match evaluation
// http://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html#_Toc325047183
enumMatch Match::evalRequest(Request request){

	std::vector<AttributeValue*> bag;

	// Any exception thrown means there was an operational error, which results in an "Indeterminate" result
	try {
		bag = evalAttributeDesignator(request);
	}
	catch (const std::exception & ex) {
		// Catch any thrown exception derived from std::exception
		// std::cerr << ex.what();
		return enumMatch::INDETERMINATE;
	}
	catch (...) {
		// Catch any other thrown exception
		return enumMatch::INDETERMINATE;
	}

	// If the bag is empty, then the result shall be "False".
	if (bag.size() == 0)
		return enumMatch::NOMATCH;

	bool atLeastOneIndeterminate = false;

	// If any of the function applications evaluate to true then the result shall be "True"
	for (int i = 0; i < bag.size(); i++) {
		enumBool currFunctionEval = evalFunction(bag, i);
		if (currFunctionEval == enumBool::TRUE)
			return enumMatch::MATCH;
		else if (currFunctionEval == enumBool::INDETERMINATE)
			atLeastOneIndeterminate = true;
	}

	// Otherwise if any evaluate to "Indeterminate" then the result is "Indeterminate"
	if (atLeastOneIndeterminate)
		return enumMatch::INDETERMINATE;

	// Otherwise the result is "False"
	return enumMatch::NOMATCH;

}

const XMLCh* Match::getMatchId() const{
	return elementAttributes.getValue(matchIdKey);
}

TwoArgBoolFunction* Match::getFunction() const{
	return function;
}

AttributeValue* Match::getAttributeValue() const{
	return attributeValue;
}

AttributeDesignator* Match::getAttributeDesignator() const{
	return attributeDesignator;
}

void Match::setMatchId(const XMLCh* matchId){
	elementAttributes.set(matchIdKey, matchId);
	function = FunctionSelector::getTwoArgBoolFunction(elementAttributes.getValue(matchIdKey));
}

void Match::setAttributeValue(AttributeValue* attributeValue){
	this->attributeValue = attributeValue;
}

void Match::setAttributeDesignator(AttributeDesignator* attributeDesignator){
	this->attributeDesignator = attributeDesignator;
}
