#include "Rule.hpp"
#include <Utils.hpp>
#include <enumBool.hpp>

const XMLCh* Rule::ruleIdKey;
const XMLCh* Rule::effectKey;

enumMatch Rule::evalTarget(Request request)
{
	// From section 5.21 : If the Rule has no Target then use the one from the Policy it belongs to
	if (target == nullptr)
		return policyTarget->evalRequest(request);
	else
		return target->evalRequest(request);
}

Rule::Rule(xercesc::DOMNode* ruleNode, Target* policyTarget){
	if (ruleIdKey == nullptr) {
		ruleIdKey = xercesc::XMLString::transcode("RuleId");
		effectKey = xercesc::XMLString::transcode("Effect");
	}

	this->policyTarget = policyTarget;

	elementAttributes = ElementAttributes(ruleNode);
	//Target tempTarget(getChildWithName(ruleNode, "Target"));
	target = new Target(getChildWithName(ruleNode, "Target"));
	//target = &tempTarget;
	XMLCh* tmp = xercesc::XMLString::transcode("Permit");
	if (xercesc::XMLString::compareString(tmp, elementAttributes.getValue(effectKey)) == 0)
		effect = enumDecision::PERMIT;
	else
		effect = enumDecision::DENY;

	xercesc::XMLString::release(&tmp);
}

enumDecision Rule::evalRequest(Request request){
	// Section "7.11 Rule evaluation" of the XACML specification

	// If the <Condition> element is absent then it evaluates to true
	enumBool conditionResult = enumBool::TRUE;
	enumMatch targetResult = evalTarget(request);

	if (targetResult == enumMatch::MATCH) {
		if (conditionResult == enumBool::FALSE)
			return enumDecision::NOTAPPLICABLE;
		else if (conditionResult == enumBool::TRUE)
			return effect;
		else {
			if (effect == enumDecision::PERMIT)
				return enumDecision::INDETERMINATE_P;
			else
				return enumDecision::INDETERMINATE_D;
		}
	}
	else if (targetResult == enumMatch::NOMATCH) {
		return enumDecision::NOTAPPLICABLE;
	}
	else { // else targetResult is INDETERMINATE
		if (effect == enumDecision::PERMIT)
			return enumDecision::INDETERMINATE_P;
		else
			return enumDecision::INDETERMINATE_D;
	}

	
}

Target* Rule::getTarget() const
{
	return target;
}

const XMLCh* Rule::getRuleId() const
{
	return elementAttributes.getValue(ruleIdKey);
}

const XMLCh* Rule::getEffect() const
{
	return elementAttributes.getValue(effectKey);
}

void Rule::setTarget(Target* target){
	this->target = target;
}

void Rule::setRuleId(const XMLCh* ruleId){
	elementAttributes.set(ruleIdKey, ruleId);
}

void Rule::setEffect(const XMLCh* effect){
	elementAttributes.set(effectKey, effect);
	XMLCh* tmp = xercesc::XMLString::transcode("Permit");
	if (xercesc::XMLString::compareString(tmp, effect) == 0)
		this->effect = enumDecision::PERMIT;
	else
		this->effect = enumDecision::DENY;

	xercesc::XMLString::release(&tmp);
}
