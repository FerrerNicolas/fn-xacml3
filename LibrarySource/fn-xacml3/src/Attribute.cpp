#include "Attribute.hpp"

#include <Utils.hpp>

const XMLCh* Attribute::attributeIdKey;
const XMLCh* Attribute::issuerKey;
const XMLCh* Attribute::includeInResultKey;

Attribute::Attribute(xercesc::DOMNode* attributeNode)
{
	if (attributeIdKey == nullptr){
		attributeIdKey = xercesc::XMLString::transcode("AttributeId");
		issuerKey = xercesc::XMLString::transcode("Issuer");
		includeInResultKey = xercesc::XMLString::transcode("IncludeInResult");
	}

	elementAttributes = ElementAttributes(attributeNode);
	xercesc::DOMNode* attributeValueNode = getChildWithName(attributeNode, "AttributeValue");
	attributeValue = new AttributeValue(attributeValueNode);
}

const XMLCh* Attribute::getAttributeId() const
{
	return elementAttributes.getValue(attributeIdKey);
}

const XMLCh* Attribute::getIssuer() const
{
	return elementAttributes.getValue(issuerKey);
}

const XMLCh* Attribute::getIncludeInResult() const
{
	return elementAttributes.getValue(includeInResultKey);
}

AttributeValue* Attribute::getAttributeValue() const
{
	return attributeValue;
}



void Attribute::setAttributeId(const XMLCh* attributeId)
{
	elementAttributes.set(attributeIdKey, attributeId);
}

void Attribute::setIssuer(const XMLCh* issuer)
{
	elementAttributes.set(issuerKey, issuer);
}

void Attribute::setIncludeInResult(const XMLCh* includeInResult)
{
	elementAttributes.set(includeInResultKey, includeInResult);
}

void Attribute::setAttributeValue(AttributeValue* attributeValue)
{
	this->attributeValue = attributeValue;
}
