#include "Target.hpp"
#include <Utils.hpp>


Target::Target(xercesc::DOMNode* targetNode){
	std::vector<xercesc::DOMNode*> anyOfElementNodes = getChildrenWithName(targetNode, "AnyOf");
	anyOf = std::vector<AnyOf*> (anyOfElementNodes.size());
	for (int i = 0; i < anyOf.size(); i++) {
		anyOf[i] = new AnyOf(anyOfElementNodes[i]);
	}

}

enumMatch Target::evalRequest(Request request){
	// Section "7.7 Target Evaluation" of the XACML specification

	// An empty taget matches any request
	if (anyOf.size() == 0)
		return enumMatch::MATCH;

	bool anyIndeterminate = false;

	// If any anyOf evaluates to "No match" then return "No match"
	for (int i = 0; i < anyOf.size(); i++) {
		enumMatch currAnyOfEval = evalAnyOf(i, request);
		if (currAnyOfEval == enumMatch::NOMATCH)
			return enumMatch::NOMATCH;
		else if (currAnyOfEval == enumMatch::INDETERMINATE)
			anyIndeterminate = true;
	}

	// If this section executes that means that there were no "No match" evals

	// If there aren't any "No match" and there's at least one Indeterminate then return Indeterminate
	if (anyIndeterminate)
		return enumMatch::INDETERMINATE;

	// Otherwise if all are "Match" then return "Match"
	return enumMatch::MATCH;
}

enumMatch Target::evalAnyOf(int index, Request request)
{
	return anyOf[index]->evalRequest(request);
}

std::vector<AnyOf*> Target::getAnyOf() const
{
	return anyOf;
}

void Target::setAnyOf(std::vector<AnyOf*> anyOf){
	this->anyOf = anyOf;
}
