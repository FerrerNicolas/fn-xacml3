#include "FunctionAnyURIEqual.hpp"
#include <xercesc/util/XMLString.hpp>

const XMLCh* FunctionAnyURIEqual::validArg1Type;// = "http://www.w3.org/2001/XMLSchema#anyURI";

const XMLCh* FunctionAnyURIEqual::validArg2Type;// = "http://www.w3.org/2001/XMLSchema#anyURI";

using namespace xercesc;

enumBool FunctionAnyURIEqual::eval(AttributeValue* arg1, AttributeValue* arg2) const {
	if (validArg1Type == nullptr) {
		validArg1Type = xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI");
		validArg2Type = xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI");
	}

	// Function type-checking

	int arg1TypeValidity = XMLString::compareString(arg1->getDataType(), validArg1Type);
	int arg2TypeValidity = XMLString::compareString(arg2->getDataType(), validArg2Type);

	if (arg1TypeValidity || arg2TypeValidity) {
		// One of the arguments is of the wrong type
		// TODO: throw exception
	}

	int result = XMLString::compareString(arg1->getValue(), arg2->getValue());

	if (result == 0) // Means both strings are equal
		return enumBool::TRUE;
	else
		return enumBool::FALSE;

}

const XMLCh* FunctionAnyURIEqual::getArgument1Type() const{
	if (validArg1Type == nullptr) {
		validArg1Type = xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI");
	}
	return validArg1Type;
}

const XMLCh* FunctionAnyURIEqual::getArgument2Type() const{
	if (validArg2Type == nullptr) {
		validArg2Type = xercesc::XMLString::transcode("http://www.w3.org/2001/XMLSchema#anyURI");
	}
	return validArg2Type;
}
