#include "Request.hpp"
#include <Utils.hpp>

const XMLCh* Request::returnPolicyIdListKey;
const XMLCh* Request::combinedDecisionKey;

void Request::initKeys()
{
	// Check if they are nullptr to avoid transcoding multiple times
	if (returnPolicyIdListKey == nullptr) {
		returnPolicyIdListKey = xercesc::XMLString::transcode("ReturnPolicyIdList");
		combinedDecisionKey = xercesc::XMLString::transcode("CombinedDecision");
	}
}

Request::Request(xercesc::DOMNode* requestNode)
{
	initKeys();
	elementAttributes = ElementAttributes(requestNode);
	std::vector<xercesc::DOMNode*> attributesNodes = getChildrenWithName(requestNode, "Attributes");
	for (int i = 0; i < attributesNodes.size(); i++) {
		attributes.push_back(new Attributes(attributesNodes[i]));
	}
}

Request::Request()
{
	initKeys();
}

const XMLCh* Request::getReturnPolicyIdList() const{
	return elementAttributes.getValue(returnPolicyIdListKey);
}

const XMLCh* Request::getCombinedDecision() const
{
	return elementAttributes.getValue(combinedDecisionKey);
}

std::vector<Attributes*> Request::getAttributes() const{
	return attributes;
}

void Request::setReturnPolicyIdList(const XMLCh* returnPolicyIdList){
	elementAttributes.set(returnPolicyIdListKey, returnPolicyIdList);
}

void Request::setCombinedDecision(const XMLCh* combinedDecision){
	elementAttributes.set(combinedDecisionKey, combinedDecision);
}

void Request::setAttributes(std::vector<Attributes*> attributes){
	this->attributes = attributes;
}
