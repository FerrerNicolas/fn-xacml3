#include "FunctionSelector.hpp"

#include <xercesc/util/XMLString.hpp>


const XMLCh* FunctionSelector::functionAnyURIEqual_URI;
FunctionAnyURIEqual FunctionSelector::functionAnyURIEqual = FunctionAnyURIEqual();


using namespace xercesc;

TwoArgBoolFunction* FunctionSelector::getTwoArgBoolFunction(const XMLCh* functionURI)
{
	if (functionAnyURIEqual_URI == nullptr) {
		functionAnyURIEqual_URI = xercesc::XMLString::transcode("urn:oasis:names:tc:xacml:1.0:function:anyURI-equal");
	}

	int differentThanFunctionAnyURIEqual = XMLString::compareIString(functionURI, functionAnyURIEqual_URI);

	if (!differentThanFunctionAnyURIEqual) {
		return &functionAnyURIEqual;
	}
	

	return &functionAnyURIEqual;
}
