#include "Policy.hpp"

#include <Utils.hpp>
#include <RuleCombiningAlgorithmSelector.hpp>

const XMLCh* Policy::policyIdKey;
const XMLCh* Policy::versionKey;
const XMLCh* Policy::ruleCombiningAlgIdKey;


enumMatch Policy::evalTarget(Request request)
{
	return target->evalRequest(request);
}

enumDecision Policy::evalCombiningAlgorithm(Request request)
{
	return ruleCombiningAlgorithm->evalRequest(rules, request);
}

Policy::Policy(xercesc::DOMNode* policyNode)
{
	if (policyIdKey == nullptr) {
		policyIdKey = xercesc::XMLString::transcode("PolicyId");
		versionKey = xercesc::XMLString::transcode("Version");
		ruleCombiningAlgIdKey = xercesc::XMLString::transcode("RuleCombiningAlgId");
	}

	elementAttributes = ElementAttributes(policyNode);

	ruleCombiningAlgorithm = RuleCombiningAlgorithmSelector::getRuleCombiningAlgorithm(elementAttributes.getValue(ruleCombiningAlgIdKey));

	target = new Target(getChildWithName(policyNode, "Target"));

	std::vector<xercesc::DOMNode*> ruleNodes = getChildrenWithName(policyNode, "Rule");
	rules = std::vector<Rule*>(ruleNodes.size());
	for (int i = 0; i < rules.size(); i++) {
		rules[i] = new Rule(ruleNodes[i], target);
	}
	
}

enumDecision Policy::evalRequest(Request request)
{
	// Section 7.12 Policy evaluation
	// http://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html#_Toc325047189

	enumMatch targetEval = evalTarget(request);

	// Check if it's a NOMATCH first to avoid evaluating the combining algorithm when unnecessary
	if (targetEval == enumMatch::NOMATCH)
		return enumDecision::NOTAPPLICABLE;
	else if (targetEval == enumMatch::MATCH)
		return evalCombiningAlgorithm(request);
	else { // target is "Indeterminate"
		// Section 7.14 Policy and Policy set value for Indeterminate Target
		// http://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html#_Toc325047191
		enumDecision combiningAlgorithmEval = evalCombiningAlgorithm(request);
		if (combiningAlgorithmEval == enumDecision::PERMIT)
			return enumDecision::INDETERMINATE_P;
		else if (combiningAlgorithmEval == enumDecision::DENY)
			return enumDecision::INDETERMINATE_D;
		else if (combiningAlgorithmEval == enumDecision::INDETERMINATE)
			return enumDecision::INDETERMINATE_DP;
		else
			return combiningAlgorithmEval;
	}
}

const XMLCh* Policy::getPolicyId() const{
	return elementAttributes.getValue(policyIdKey);
}

const XMLCh* Policy::getVersion() const{
	return elementAttributes.getValue(versionKey);
}

const XMLCh* Policy::getRuleCombiningAlgId() const{
	return elementAttributes.getValue(ruleCombiningAlgIdKey);
}

RuleCombiningAlgorithm* Policy::getRuleCombiningAlgorithm() const{
	return ruleCombiningAlgorithm;
}

Target* Policy::getTarget() const{
	return target;
}

std::vector<Rule*> Policy::getRules() const{
	return rules;
}

void Policy::setPolicyId(const XMLCh* policyId){
	elementAttributes.set(policyIdKey, policyId);
}

void Policy::setVersion(const XMLCh* version){
	elementAttributes.set(versionKey, version);
}

void Policy::setRuleCombiningAlgId(const XMLCh* ruleCombiningAlgId){
	elementAttributes.set(ruleCombiningAlgIdKey, ruleCombiningAlgId);
	ruleCombiningAlgorithm = RuleCombiningAlgorithmSelector::getRuleCombiningAlgorithm(elementAttributes.getValue(ruleCombiningAlgIdKey));
}

void Policy::setTarget(Target* target){
	this->target = target;
}

void Policy::setRules(std::vector<Rule*> rules){
	this->rules = rules;
}
