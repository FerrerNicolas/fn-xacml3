#include "Utils.hpp"
#include <xercesc/dom/DOMNodeList.hpp>

xercesc::DOMNode* getChildWithName(xercesc::DOMNode* parent, XMLCh* name) {
	xercesc::DOMNodeList* children = parent->getChildNodes();
	for (int i = 0; i < children->getLength(); i++) {
		xercesc::DOMNode* currChild = children->item(i);
		const XMLCh* currChildName = currChild->getNodeName();
		if (xercesc::XMLString::compareString(currChildName, name) == 0) {
			return currChild;
		}
	}
	return nullptr;
}

xercesc::DOMNode* getChildWithName(xercesc::DOMNode* parent, std::string name)
{
	XMLCh* name_transcoded = xercesc::XMLString::transcode(name.c_str());
	xercesc::DOMNode* result = getChildWithName(parent, name_transcoded);
	xercesc::XMLString::release(&name_transcoded);
	return result;
}

std::vector<xercesc::DOMNode*> getChildrenWithName(xercesc::DOMNode* parent, XMLCh* name)
{
	std::vector<xercesc::DOMNode*> result;
	xercesc::DOMNodeList* children = parent->getChildNodes();
	for (int i = 0; i < children->getLength(); i++) {
		xercesc::DOMNode* currChild = children->item(i);
		const XMLCh* currChildName = currChild->getNodeName();
		if (xercesc::XMLString::compareString(currChildName, name) == 0) {
			result.push_back(currChild);
		}
	}

	return result;
}

std::vector<xercesc::DOMNode*> getChildrenWithName(xercesc::DOMNode* parent, std::string name)
{
	XMLCh* name_transcoded = xercesc::XMLString::transcode(name.c_str());
	std::vector<xercesc::DOMNode*> result = getChildrenWithName(parent, name_transcoded);
	xercesc::XMLString::release(&name_transcoded);
	return result;
}


#include <xercesc/dom/DOM.hpp>
#include <iostream>

std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > simpleXMLFileParse(const char* xmlFile) {
	xercesc::XercesDOMParser* parser = new xercesc::XercesDOMParser();
	parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);
	parser->setDoNamespaces(true);

	xercesc::ErrorHandler* errHandler = (xercesc::ErrorHandler*) new xercesc::HandlerBase();
	parser->setErrorHandler(errHandler);

	try {
		parser->parse(xmlFile);
	}
	catch (const xercesc::XMLException & toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.getMessage());
		std::cout << "Exception message is: \n"
			<< message << "\n";
		xercesc::XMLString::release(&message);
		return { nullptr, nullptr };
	}
	catch (const xercesc::DOMException & toCatch) {
		char* message = xercesc::XMLString::transcode(toCatch.msg);
		std::cout << "Exception message is: \n"
			<< message << "\n";
		xercesc::XMLString::release(&message);
		return { nullptr, nullptr };
	}
	catch (...) {
		std::cout << "Unexpected Exception \n";
		return { nullptr, nullptr };
	}

	return { parser, errHandler };

}

bool XMLChEqualsString(const XMLCh* xmlch, std::string str) {
	XMLCh* transcodedString = xercesc::XMLString::transcode(str.c_str());
	int result = xercesc::XMLString::compareString(xmlch, transcodedString);
	xercesc::XMLString::release(&transcodedString);
	// compareString returns 0 when both strings are equal
	return result == 0;
}