#include "AllOf.hpp"
#include <Utils.hpp>


enumMatch AllOf::evalMatch(Request request, int index)
{
	return match[index]->evalRequest(request);
}

AllOf::AllOf(xercesc::DOMNode* allOfNode){
	std::vector<xercesc::DOMNode*> matchElementNodes = getChildrenWithName(allOfNode, "Match");
	match = std::vector<Match*> (matchElementNodes.size());
	for (int i = 0; i < match.size(); i++) {
		match[i] = new Match(matchElementNodes[i]);
	}

}

enumMatch AllOf::evalRequest(Request request){
	// Section "7.7 Target Evaluation" of the XACML specification

	bool anyIndeterminate = false;

	// If any evaluate to false then return "No match"
	for (int i = 0; i < match.size(); i++) {
		enumMatch currMatchEval = evalMatch(request, i);
		if (currMatchEval == enumMatch::NOMATCH)
			return enumMatch::NOMATCH;
		else if (currMatchEval == enumMatch::INDETERMINATE)
			anyIndeterminate = true;
	}

	// If this section executes that means that there were no false evals

	// If there aren't any false and there's at least one Indeterminate then return Indeterminate
	if (anyIndeterminate)
		return enumMatch::INDETERMINATE;

	// Otherwise if all are true then return true
	return enumMatch::MATCH;
}

std::vector<Match*> AllOf::getMatch() const{
	return match;
}

void AllOf::setMatch(std::vector<Match*> match)
{
	this->match = match;
}
