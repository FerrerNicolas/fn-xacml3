#include "AttributeValue.hpp"
#include <Utils.hpp>

const XMLCh* AttributeValue::dataTypeKey;


AttributeValue::AttributeValue()
{
	dataTypeKey = xercesc::XMLString::transcode("DataType");
	value = nullptr;
	elementAttributes.set(dataTypeKey, nullptr);
}

AttributeValue::AttributeValue(xercesc::DOMNode* attributeValueNode)
{
	dataTypeKey = xercesc::XMLString::transcode("DataType");

	// Text value of a node is found in the value of the child node of name "#text"
	value = getChildWithName(attributeValueNode, "#text")->getNodeValue();

	elementAttributes = ElementAttributes(attributeValueNode);
}

const XMLCh* AttributeValue::getDataType() const{
	return elementAttributes.getValue(dataTypeKey);
}

const XMLCh* AttributeValue::getValue() const{
	return value;
}

void AttributeValue::setDataType(const XMLCh* dataType)
{
	elementAttributes.set(dataTypeKey, dataType);
}

void AttributeValue::setValue(const XMLCh* value)
{
	this->value = value;
}
