#include "ElementAttributes.hpp"
#include <xercesc/dom/DOMNamedNodeMap.hpp>

ElementAttributes::ElementAttributes(xercesc::DOMNode* elementNode){
	xercesc::DOMNamedNodeMap* attributes = elementNode->getAttributes();
	for (int i = 0; i < attributes->getLength(); i++) {
		xercesc::DOMNode* currAttribute = attributes->item(i);
		const XMLCh* currAttributeName = currAttribute->getNodeName();
		const XMLCh* currAttributeValue = currAttribute->getNodeValue();

		attributeMap[currAttributeName] = currAttributeValue;

		/*
		char* currAttributeNameTranscoded = xercesc::XMLString::transcode(currAttributeName);
		char* currAttributeValueTranscoded = xercesc::XMLString::transcode(currAttributeValue);
		cout << "Attribute " << i << endl;
		cout << "	Name: " << currAttributeNameTranscoded << endl;
		cout << "	Value: " << currAttributeValueTranscoded << endl;
		cout << endl;
		*/
	}

}

bool ElementAttributes::set(const XMLCh* key, const XMLCh* value){

	attributeMap[key] = value;

	return true;
}

const XMLCh* ElementAttributes::getValue(const XMLCh* key) const{
	if (attributeMap.count(key)) // returns 1 if the key exist; 0 otherwise
		return attributeMap.at(key);
	else
		return nullptr;
}
