#include "RuleCombiningAlgorithmSelector.hpp"

//const XMLCh* RuleCombiningAlgorithmSelector::combiningAlgorithmDenyOverrides_Id = xercesc::XMLString::transcode("urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-overrides");

DenyOverridesCombiningAlgorithm RuleCombiningAlgorithmSelector::denyOverridesCombiningAlgorithm = DenyOverridesCombiningAlgorithm();

RuleCombiningAlgorithm* RuleCombiningAlgorithmSelector::getRuleCombiningAlgorithm(const XMLCh* combiningAlgorithm_Id)
{
	const XMLCh* combiningAlgorithmDenyOverrides_Id = xercesc::XMLString::transcode("urn:oasis:names:tc:xacml:3.0:rule-combining-algorithm:deny-overrides");

	if (xercesc::XMLString::compareString(combiningAlgorithm_Id, combiningAlgorithmDenyOverrides_Id) == 0)
		return &denyOverridesCombiningAlgorithm;

	return nullptr;
}
