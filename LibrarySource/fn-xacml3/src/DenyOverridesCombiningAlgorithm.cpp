#include "DenyOverridesCombiningAlgorithm.hpp"

enumDecision DenyOverridesCombiningAlgorithm::evalRule(std::vector<Rule*> ruleVector, Request request, int index) const
{
	return ruleVector[index]->evalRequest(request);
}

enumDecision DenyOverridesCombiningAlgorithm::evalRequest(std::vector<Rule*> ruleVector, Request request) const
{
	// Section C.2 Deny-overrides
	// http://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html#_Toc325047270

	bool atLeastOneErrorD = false;
	bool atLeastOneErrorP = false;
	bool atLeastOneErrorDP = false;
	bool atLeastOnePermit = false;

	for (int i = 0; i < ruleVector.size(); i++) {
		enumDecision currRuleDecision = evalRule(ruleVector, request, i);
		if (currRuleDecision == enumDecision::DENY)
			return enumDecision::DENY;
		else if (currRuleDecision == enumDecision::PERMIT) {
			atLeastOnePermit = true;
			continue;
		}
		else if (currRuleDecision == enumDecision::NOTAPPLICABLE) {
			continue;
		}
		else if (currRuleDecision == enumDecision::INDETERMINATE_D) {
			atLeastOneErrorD = true;
			continue;
		}
		else if (currRuleDecision == enumDecision::INDETERMINATE_P) {
			atLeastOneErrorP = true;
			continue;
		}
		else if (currRuleDecision == enumDecision::INDETERMINATE_DP) {
			atLeastOneErrorDP = true;
			continue;
		}
	}

	if (atLeastOneErrorDP)
		return enumDecision::INDETERMINATE_DP;
	else if (atLeastOneErrorD && (atLeastOneErrorP || atLeastOnePermit))
		return enumDecision::INDETERMINATE_DP;
	else if (atLeastOneErrorD)
		return enumDecision::INDETERMINATE_D;
	else if (atLeastOnePermit)
		return enumDecision::PERMIT;
	else if (atLeastOneErrorP)
		return enumDecision::INDETERMINATE_P;


	return enumDecision::NOTAPPLICABLE;
}
