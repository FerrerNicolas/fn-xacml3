#include "AttributeDesignator.hpp"
#include "Attribute.hpp"

const XMLCh* AttributeDesignator::categoryKey;
const XMLCh* AttributeDesignator::attributeIdKey;
const XMLCh* AttributeDesignator::dataTypeKey;
const XMLCh* AttributeDesignator::issuerKey;
const XMLCh* AttributeDesignator::mustBePresentKey;



AttributeDesignator::AttributeDesignator(xercesc::DOMNode* attributeDesignatorNode){
	if (categoryKey == nullptr) {
		categoryKey = xercesc::XMLString::transcode("Category");
		attributeIdKey = xercesc::XMLString::transcode("AttributeId");
		dataTypeKey = xercesc::XMLString::transcode("DataType");
		issuerKey = xercesc::XMLString::transcode("Issuer");
		mustBePresentKey = xercesc::XMLString::transcode("MustBePresent");
	}

	elementAttributes = ElementAttributes(attributeDesignatorNode);
}

std::vector<AttributeValue*> AttributeDesignator::getAttributeValues(Request request) const {
	std::vector<AttributeValue*> result;
	std::vector<Attributes*> attributesFromRequest = request.getAttributes();
	
	for (int i = 0; i < attributesFromRequest.size(); i++) {
		// The categories MUST match by identifier equality
		if (xercesc::XMLString::compareString(attributesFromRequest[i]->getCategory(), this->getCategory()) != 0)
			continue;

		// TODO: refactor for vector of Attribute*
		Attribute* attribute = attributesFromRequest[i]->getAttribute();
		
		// The attributeIds MUST match by identifier equality
		if (xercesc::XMLString::compareString(attribute->getAttributeId(), this->getAttributeId()) != 0)
			continue;

		AttributeValue* attributeValue = attribute->getAttributeValue();

		// The dataTypes MUST match by identifier equality
		if (xercesc::XMLString::compareString(attributeValue->getDataType(), this->getDataType()) != 0)
			continue;

		// TODO: Add issuer check
		// const XMLCh* issuer = attr->getIssuer();

		result.push_back(attributeValue);
		
	}


	return result;
}

const XMLCh* AttributeDesignator::getCategory() const {
	return elementAttributes.getValue(categoryKey);
}

const XMLCh* AttributeDesignator::getAttributeId() const {
	return elementAttributes.getValue(attributeIdKey);
}

const XMLCh* AttributeDesignator::getDataType() const {
	return elementAttributes.getValue(dataTypeKey);
}

const XMLCh* AttributeDesignator::getIssuer() const {
	return elementAttributes.getValue(issuerKey);
}

const XMLCh* AttributeDesignator::getMustBePresent() const {
	return elementAttributes.getValue(mustBePresentKey);
}

void AttributeDesignator::setCategory(const XMLCh* category)
{
	elementAttributes.set(categoryKey, category);
}

void AttributeDesignator::setAttributeId(const XMLCh* attributeId)
{
	elementAttributes.set(attributeIdKey, attributeId);
}

void AttributeDesignator::setDataType(const XMLCh* dataType)
{
	elementAttributes.set(dataTypeKey, dataType);
}

void AttributeDesignator::setIssuer(const XMLCh* issuer)
{
	elementAttributes.set(issuerKey, issuer);
}

void AttributeDesignator::setMustBePresent(const XMLCh* mustBePresent)
{
	elementAttributes.set(mustBePresentKey, mustBePresent);
}
