#include "AnyOf.hpp"
#include <Utils.hpp>
#include <vector>

enumMatch AnyOf::evalAllOf(Request request, int index) const
{
	return allOf[index]->evalRequest(request);
}

AnyOf::AnyOf(xercesc::DOMNode* anyOfNode){
	std::vector<xercesc::DOMNode*> allOfElementNodes = getChildrenWithName(anyOfNode, "AllOf");
	allOf = std::vector<AllOf*> (allOfElementNodes.size());
	for (int i = 0; i < allOf.size(); i++) {
		allOf[i] = new AllOf(allOfElementNodes[i]);
	}

}

enumMatch AnyOf::evalRequest(Request request) const{
	// Section "7.7 Target Evaluation" of the XACML specification

	bool anyIndeterminate = false;

	// If any allOf evaluate to Match then return Match
	for (int i = 0; i < allOf.size(); i++) {
		enumMatch currAllOfEval = evalAllOf(request, i);
		if (currAllOfEval == enumMatch::MATCH)
			return enumMatch::MATCH;
		if (currAllOfEval == enumMatch::INDETERMINATE)
			anyIndeterminate = true;
	}

	// If this section executes then no evaluation resulted in a Match

	// If none matches and there's at least one indeterminate then return Indeterminate
	if (anyIndeterminate)
		return enumMatch::INDETERMINATE;

	// Otherwise if all are "No match" then return "No match"
	return enumMatch::NOMATCH;
}

std::vector<AllOf*> AnyOf::getAllOf() const{
	return allOf;
}

void AnyOf::setAllOf(std::vector<AllOf*> allOf)
{
	this->allOf = allOf;
}
