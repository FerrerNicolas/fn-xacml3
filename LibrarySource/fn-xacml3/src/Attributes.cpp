#include "Attributes.hpp"
#include <Utils.hpp>


const XMLCh* Attributes::categoryKey;

Attributes::Attributes(xercesc::DOMNode* attributesNode){
	if (categoryKey == nullptr) {
		categoryKey = xercesc::XMLString::transcode("Category");
	}

	elementAttributes = ElementAttributes(attributesNode);
	xercesc::DOMNode* attributeNode = getChildWithName(attributesNode, "Attribute");
	if (attributeNode != nullptr)
		attribute = new Attribute(attributeNode);
}

const XMLCh* Attributes::getCategory() const{
	return elementAttributes.getValue(categoryKey);
}

Attribute* Attributes::getAttribute() const{
	return attribute;
}

void Attributes::setCategory(const XMLCh* category)
{
	elementAttributes.set(categoryKey, category);
}

void Attributes::setAttribute(Attribute* attribute)
{
	this->attribute = attribute;
}
