#pragma once

#ifndef UTILS_H
#define UTILS_H

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <string>
#include <vector>

xercesc::DOMNode* getChildWithName(xercesc::DOMNode* parent, XMLCh* name);
xercesc::DOMNode* getChildWithName(xercesc::DOMNode* parent, std::string name);

std::vector<xercesc::DOMNode*> getChildrenWithName(xercesc::DOMNode* parent, XMLCh* name);
std::vector<xercesc::DOMNode*> getChildrenWithName(xercesc::DOMNode* parent, std::string name);

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/sax/HandlerBase.hpp>

std::pair< xercesc::XercesDOMParser*, xercesc::ErrorHandler* > simpleXMLFileParse(const char* xmlFile);

bool XMLChEqualsString(const XMLCh* xmlch, std::string str);

#endif