#pragma once

#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <AttributeValue.hpp>
#include <ElementAttributes.hpp>

class Attribute {

	static const XMLCh* attributeIdKey;
	static const XMLCh* issuerKey;
	static const XMLCh* includeInResultKey;
	AttributeValue* attributeValue;
	ElementAttributes elementAttributes;

public:

	Attribute(xercesc::DOMNode* attributeNode);

	const XMLCh* getAttributeId() const;
	const XMLCh* getIssuer() const;
	const XMLCh* getIncludeInResult() const;
	AttributeValue* getAttributeValue() const;

	void setAttributeId(const XMLCh* attributeId);
	void setIssuer(const XMLCh* issuer);
	void setIncludeInResult(const XMLCh* includeInResult);
	void setAttributeValue(AttributeValue* attributeValue);
	
};

#endif