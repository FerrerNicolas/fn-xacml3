#pragma once

#ifndef TWOARGBOOLFUNCTION_H
#define TWOARGBOOLFUNCTION_H

#include <string>
#include <xercesc/util/XMLString.hpp>
#include <AttributeValue.hpp>
#include <enumBool.hpp>

class TwoArgBoolFunction {

public:
	virtual enumBool eval(AttributeValue* arg1, AttributeValue* arg2) const = 0;
	virtual const XMLCh* getArgument1Type() const = 0;
	virtual const XMLCh* getArgument2Type() const = 0;
};

#endif