#pragma once

#ifndef FUNCTIONSELECTOR_H
#define FUNCTIONSELECTOR_H

#include <xercesc/util/XMLString.hpp>
#include <TwoArgBoolFunction.hpp>

// The various Two Argument Boolean Functions
#include <FunctionAnyURIEqual.hpp>

class FunctionSelector {
	static const XMLCh* functionAnyURIEqual_URI;
	static FunctionAnyURIEqual functionAnyURIEqual;

public:
	static TwoArgBoolFunction* getTwoArgBoolFunction(const XMLCh* functionURI);

};

#endif