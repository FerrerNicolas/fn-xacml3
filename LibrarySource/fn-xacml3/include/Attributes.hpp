#pragma once

#ifndef ATTRIBUTES_H
#define ATTRIBUTES_H

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <Attribute.hpp>
#include <ElementAttributes.hpp>

class Attributes {

	static const XMLCh* categoryKey;
	Attribute* attribute;
	ElementAttributes elementAttributes;

public:

	Attributes(xercesc::DOMNode* attributesNode);

	const XMLCh* getCategory() const;
	Attribute* getAttribute() const;

	void setCategory(const XMLCh* category);
	void setAttribute(Attribute* attribute);
	
};

#endif