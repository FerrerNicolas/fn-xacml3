#pragma once

#ifndef ELEMENTATTRIBUTES_H
#define ELEMENTATTRIBUTES_H

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMNode.hpp>

#include <map>


class ElementAttributes {
	struct compLessXMLChP {

		bool operator()(const XMLCh* a,const XMLCh* b) const{
			return xercesc::XMLString::compareString(a, b) < 0;
		}

	};

	std::map<const XMLCh*,const XMLCh*, compLessXMLChP> attributeMap;

public:
	ElementAttributes() = default;
	ElementAttributes(xercesc::DOMNode* elementNode);
	bool set(const XMLCh* key,const XMLCh* value);
	const XMLCh* getValue(const XMLCh* key) const;
	
};

#endif