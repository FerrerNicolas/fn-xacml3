#pragma once

#ifndef FUNCTIONANYURIEQUAL_H
#define FUNCTIONANYURIEQUAL_H

#include <xercesc/util/XMLString.hpp>
#include <TwoArgBoolFunction.hpp>

class FunctionAnyURIEqual : public TwoArgBoolFunction {

	static const XMLCh* validArg1Type; // = "http://www.w3.org/2001/XMLSchema#anyURI";
	static const XMLCh* validArg2Type; // = "http://www.w3.org/2001/XMLSchema#anyURI";

public:
	enumBool eval(AttributeValue* arg1, AttributeValue* arg2) const;
	const XMLCh* getArgument1Type() const;
	const XMLCh* getArgument2Type() const;
	
};

#endif