#pragma once

#ifndef REQUEST_H
#define REQUEST_H

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <Attributes.hpp>
#include <ElementAttributes.hpp>
#include <vector>

class Request {

	static const XMLCh* returnPolicyIdListKey;
	static const XMLCh* combinedDecisionKey;
	std::vector<Attributes*> attributes;
	ElementAttributes elementAttributes;

	void initKeys();

public:
	
	Request(xercesc::DOMNode* requestNode);
	Request();

	const XMLCh* getReturnPolicyIdList() const;
	const XMLCh* getCombinedDecision() const;
	std::vector<Attributes*> getAttributes() const;

	void setReturnPolicyIdList(const XMLCh* returnPolicyIdList);
	void setCombinedDecision(const XMLCh* combinedDecision);
	void setAttributes(std::vector<Attributes*> attributes);

};

#endif