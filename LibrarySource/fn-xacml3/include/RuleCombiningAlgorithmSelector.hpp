#pragma once

#ifndef RULECOMBININGALGORITHMSELECTOR_H
#define RULECOMBININGALGORITHMSELECTOR_H

#include <xercesc/util/XMLString.hpp>
#include <RuleCombiningAlgorithm.hpp>
#include <DenyOverridesCombiningAlgorithm.hpp>

class RuleCombiningAlgorithmSelector {
	
	//static const XMLCh* combiningAlgorithmDenyOverrides_Id;
	static DenyOverridesCombiningAlgorithm denyOverridesCombiningAlgorithm;

public:
	static RuleCombiningAlgorithm* getRuleCombiningAlgorithm(const XMLCh* combiningAlgorithm_Id);

};

#endif