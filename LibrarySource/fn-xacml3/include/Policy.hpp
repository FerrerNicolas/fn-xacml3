#pragma once

#ifndef POLICY_H
#define POLICY_H

#include <xercesc/dom/DOMNode.hpp>
#include <Rule.hpp>
#include <Request.hpp>
#include <RuleCombiningAlgorithm.hpp>
#include <ElementAttributes.hpp>
#include <enumDecision.hpp>

class Policy {
	static const XMLCh* policyIdKey;
	static const XMLCh* versionKey;
	static const XMLCh* ruleCombiningAlgIdKey;
	RuleCombiningAlgorithm* ruleCombiningAlgorithm;
	Target* target;
	std::vector<Rule*> rules;
	ElementAttributes elementAttributes;

// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	virtual enumMatch evalTarget(Request request);
	virtual enumDecision evalCombiningAlgorithm(Request request);
	Policy() = default;

public:
	Policy(xercesc::DOMNode* policyNode);

	enumDecision evalRequest(Request request);

	const XMLCh* getPolicyId() const;
	const XMLCh* getVersion() const;
	const XMLCh* getRuleCombiningAlgId() const;
	RuleCombiningAlgorithm* getRuleCombiningAlgorithm() const;
	Target* getTarget() const;
	std::vector<Rule*> getRules() const;

	void setPolicyId(const XMLCh* policyId);
	void setVersion(const XMLCh* version);
	void setRuleCombiningAlgId(const XMLCh* ruleCombiningAlgId);
	void setTarget(Target* target);
	void setRules(std::vector<Rule*> rules);
	
};

#endif