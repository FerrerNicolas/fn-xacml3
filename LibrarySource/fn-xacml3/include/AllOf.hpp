#pragma once

#ifndef ALLOF_H
#define ALLOF_H

#include <xercesc/dom/DOMNode.hpp>
#include <Match.hpp>
#include <vector>
#include <enumMatch.hpp>

class AllOf {
	
// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	std::vector<Match*> match;
	virtual enumMatch evalMatch(Request request, int index);
	AllOf() = default;

public:
	AllOf(xercesc::DOMNode* allOfNode);

	enumMatch evalRequest(Request request);

	std::vector<Match*> getMatch() const;

	void setMatch(std::vector<Match*> match);
	
};

#endif