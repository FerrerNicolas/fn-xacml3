#pragma once

#ifndef MATCH_H
#define MATCH_H

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <TwoArgBoolFunction.hpp>
#include <AttributeDesignator.hpp>
#include <AttributeValue.hpp>
#include <enumMatch.hpp>

// Section 5.9 Element <Match>
// http://docs.oasis-open.org/xacml/3.0/xacml-3.0-core-spec-os-en.html#_Toc325047114
class Match {
	static const XMLCh* matchIdKey;
	TwoArgBoolFunction* function;
	AttributeValue* attributeValue;
	AttributeDesignator* attributeDesignator;
	ElementAttributes elementAttributes;

	void initKeys();

// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	virtual std::vector<AttributeValue*> evalAttributeDesignator(Request request);
	virtual enumBool evalFunction(std::vector<AttributeValue*> bag, int index);


public:
	Match(xercesc::DOMNode* matchNode);
	Match();

	enumMatch evalRequest(Request request);

	const XMLCh* getMatchId() const;
	TwoArgBoolFunction* getFunction() const;
	AttributeValue* getAttributeValue() const;
	AttributeDesignator* getAttributeDesignator() const;

	void setMatchId(const XMLCh* matchId);
	void setAttributeValue(AttributeValue* attributeValue);
	void setAttributeDesignator(AttributeDesignator* attributeDesignator);
	
	
};

#endif