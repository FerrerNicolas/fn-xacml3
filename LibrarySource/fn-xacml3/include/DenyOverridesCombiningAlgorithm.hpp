#pragma once

#ifndef DENYOVERRIDESCOMBININGALGORITHM_H
#define DENYOVERRIDESCOMBININGALGORITHM_H

#include <vector>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/util/XMLString.hpp>
#include <RuleCombiningAlgorithm.hpp>
#include <Rule.hpp>

class DenyOverridesCombiningAlgorithm : public RuleCombiningAlgorithm {

// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	virtual enumDecision evalRule(std::vector<Rule*> ruleVector, Request request, int index) const;

public:
	enumDecision evalRequest(std::vector<Rule*> ruleVector, Request request) const;

};

#endif