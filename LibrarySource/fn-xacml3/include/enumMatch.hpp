#pragma once

#ifndef ENUMMATCH_H
#define ENUMMATCH_H

enum class enumMatch {
	MATCH,
	NOMATCH,
	INDETERMINATE,
};


#endif