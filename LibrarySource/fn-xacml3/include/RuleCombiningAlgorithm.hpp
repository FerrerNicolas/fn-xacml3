#pragma once

#ifndef RULECOMBININGALGORITHM_H
#define RULECOMBININGALGORITHM_H

#include <vector>
#include <Rule.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <enumDecision.hpp>

class RuleCombiningAlgorithm {

// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	virtual enumDecision evalRule(std::vector<Rule*> ruleVector, Request request, int index) const = 0;

public:
	virtual enumDecision evalRequest(std::vector<Rule*> ruleVector, Request request) const = 0;
	
};

#endif