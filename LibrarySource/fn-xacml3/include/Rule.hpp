#pragma once

#ifndef RULE_H
#define RULE_H

#include <xercesc/dom/DOMNode.hpp>
#include <Target.hpp>
#include <enumDecision.hpp>

class Rule {
	Target* target;
	// Second differentiated target for the case in which a Rule has no target and has to use the one from the Policy it belongs to
	Target* policyTarget;
	static const XMLCh* ruleIdKey;
	static const XMLCh* effectKey;
	enumDecision effect;
	ElementAttributes elementAttributes;

// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	virtual enumMatch evalTarget(Request request);
	Rule() = default;

public:
	Rule(xercesc::DOMNode* ruleNode, Target* policyTarget);

	enumDecision evalRequest(Request request);

	Target* getTarget() const;
	const XMLCh* getRuleId() const;
	const XMLCh* getEffect() const;

	void setTarget(Target* target);
	void setRuleId(const XMLCh* ruleId);
	void setEffect(const XMLCh* effect);
	
};

#endif