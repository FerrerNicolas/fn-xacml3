#pragma once

#ifndef ENUMBOOL_H
#define ENUMBOOL_H

enum class enumBool {
	FALSE,
	TRUE,
	INDETERMINATE
};


#endif