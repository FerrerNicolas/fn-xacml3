#pragma once

#ifndef ENUMDECISION_H
#define ENUMDECISION_H

enum class enumDecision {
	PERMIT,
	DENY,
	INDETERMINATE,
	NOTAPPLICABLE,
	INDETERMINATE_D,
	INDETERMINATE_P,
	INDETERMINATE_DP
};


#endif