#pragma once

#ifndef ANYOF_H
#define ANYOF_H

#include <xercesc/dom/DOMNode.hpp>
#include <AllOf.hpp>
#include <enumMatch.hpp>

class AnyOf {
	
// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	std::vector<AllOf*> allOf;
	virtual enumMatch evalAllOf(Request request, int index) const;
	AnyOf() = default;

public:
	AnyOf(xercesc::DOMNode* anyOfNode);
	

	enumMatch evalRequest(Request request) const;

	std::vector<AllOf*> getAllOf() const;

	void setAllOf(std::vector<AllOf*> allOf);
	
};

#endif