#pragma once

#ifndef ATTRIBUTEVALUE_H
#define ATTRIBUTEVALUE_H

#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <ElementAttributes.hpp>

class AttributeValue {

	static const XMLCh* dataTypeKey;
	const XMLCh* value;
	ElementAttributes elementAttributes;
public:

	AttributeValue();
	AttributeValue(xercesc::DOMNode* attributeValueNode);

	const XMLCh* getDataType() const;
	const XMLCh* getValue() const;

	void setDataType(const XMLCh* dataType);
	void setValue(const XMLCh* value);


};

#endif