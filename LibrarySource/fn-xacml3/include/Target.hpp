#pragma once

#ifndef TARGET_H
#define TARGET_H

#include <xercesc/dom/DOMNode.hpp>
#include <AnyOf.hpp>
#include <Request.hpp>
#include <vector>
#include <enumMatch.hpp>

class Target {

// Protected elements so their characteristics can be faked in testing via inheritance
protected:
	std::vector<AnyOf*> anyOf;
	virtual enumMatch evalAnyOf(int index, Request request);
	Target() = default;

public:
	Target(xercesc::DOMNode* targetNode);
	

	enumMatch evalRequest(Request request);

	std::vector<AnyOf*> getAnyOf() const;
	void setAnyOf(std::vector<AnyOf*> anyOf);

};

#endif