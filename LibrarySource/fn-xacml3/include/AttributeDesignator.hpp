#pragma once

#ifndef ATTRIBUTEDESIGNATOR_H
#define ATTRIBUTEDESIGNATOR_H

#include <Request.hpp>
#include <ElementAttributes.hpp>

#include <xercesc/util/XMLString.hpp>
#include <vector>
#include <xercesc/dom/DOMNode.hpp>


class AttributeDesignator {
	static const XMLCh* categoryKey;
	static const XMLCh* attributeIdKey;
	static const XMLCh* dataTypeKey;
	static const XMLCh* issuerKey; // optional
	static const XMLCh* mustBePresentKey;
	ElementAttributes elementAttributes;

public:
	AttributeDesignator(xercesc::DOMNode* attributeDesignatorNode);

	std::vector<AttributeValue*> getAttributeValues(Request request) const;

	const XMLCh* getCategory() const;
	const XMLCh* getAttributeId() const;
	const XMLCh* getDataType() const;
	const XMLCh* getIssuer() const;
	const XMLCh* getMustBePresent() const;

	void setCategory(const XMLCh* category);
	void setAttributeId(const XMLCh* attributeId);
	void setDataType(const XMLCh* dataType);
	void setIssuer(const XMLCh* issuer);
	void setMustBePresent(const XMLCh* mustBePresent);

};

#endif