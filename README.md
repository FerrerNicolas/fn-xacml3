# FN-XACML3
C++ implementation of XACML 3.0

For the build process of xerces cmake will be required, which can be downloaded at: https://cmake.org/download/

To Build Xerces:

Go into the ThirdPartyLibs folder and create a folder named "xercesBuild".

 Then extract xerces-c-3.2.3.zip in the ThirdPartyLibs folder (this should result in a folder named xerces-c-3.2.3 containing the xerces src folder, etc) resulting in the following file structure:
 
ThirdPartyLibs
		|---- xercesBuild
		|---- xerces-c-3.2.3

Afterwards navitgate into the xerces-c-3.2.3 folder and execute the following command lines:
1) cmake -DCMAKE_INSTALL_PREFIX="..\xercesBuild"
2) cmake --build . --config Debug --target install -DCMAKE_INSTALL_PREFIX="..\xercesBuild"